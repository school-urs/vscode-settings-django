# Шаблон настроек: VSCode, Django, Poetry

## Общая информация и настройка окружения

Настройки рабочей области VSCode для Django проекта с использованием:

- Python3 (3.10)
- Django (4.2)
- Poetry
- Black

### Установка Poetry

[Выбор версии Python для Poetry](https://devnote.ural-site.ru/texnologii/python-django-drf/python/instrumentyi-i-moduli/poetry/)

[Спецификация зависимостей](https://python-poetry.org/docs/dependency-specification/)

Перед началом установки зависимостей необходимо убедиться в том, что устанвлен Poetry, для этого проверьте версию.

```bash
poetry --version
```

Если Poetry не установлен, следует установить его:

```bash
pip install poetry
```

### Установка зависимостей

Устанавливаем зависимости:

```bash
poetry install
```

## Запуск Django проекта

### Создание проекта

Точка в конце знчит создать в текущей директории. "nameproject" - это имя проекта, возможно также для удобства имя проекта указать - "config"

```bash
poetry run django-admin startproject nameproject .
```

### Запустить сервер разработки

```bash
# Применить миграции
./manage.py migrate

# Запуситить сервер разработки
./manage.py runserver

# Создать суперюзера
./manage.py createsuperuser
```
